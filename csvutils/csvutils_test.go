package csvutils

import (
	"testing"
)

func TestReadCsvFile(t *testing.T) {
	got := ReadCsvFile("test_resources/test.csv")
	want := [][]string{
		{"a", "b"},
		{"1", "2"},
	}

	// TODO: Check why reflect.DeepEqual is not working here
	for i := range got {
		for j := range got[i] {
			if got[i][j] != want[i][j] {
				t.Errorf("got %q, wanted %q", got, want)
			}
		}
	}
}
