package csvutils

import (
	"encoding/csv"
	"fmt"
	"os"
)

// ReadCsvFile reads a CSV file and returns a two-dimensional array
func ReadCsvFile(path string) [][]string {
	// Open the CSV file
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return nil
	}
	defer file.Close()

	// Read the CSV data
	reader := csv.NewReader(file)
	reader.Comma = '|' // Use pipe-delimited instead of comma <---- here!
	reader.LazyQuotes = true
	records, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Error reading CSV:", err)
		return nil
	}

	// Create a two-dimensional array to store the values
	rows := len(records)
	cols := len(records[0])
	arr := make([][]string, rows)
	for i := range arr {
		arr[i] = make([]string, cols)
	}

	// Insert values into the array
	for i, row := range records {
		for j, value := range row {
			arr[i][j] = value
		}
	}

	// Print the array
	//for _, row := range arr {
	//	fmt.Println(row)
	//}
	return arr
}

// WriteCsvFile writes a two-dimensional array into a CSV file
func WriteCsvFile(path string, arr [][]string) {
	// Create the CSV file
	os.ReadDir(path)
	file, err := os.Create(path)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	// Write the CSV data
	writer := csv.NewWriter(file)
	writer.Comma = '|' // Use pipe-delimited instead of comma <---- here!
	defer writer.Flush()
	for _, row := range arr {
		err := writer.Write(row)
		if err != nil {
			fmt.Println("Error writing CSV:", err)
			return
		}
	}
}
