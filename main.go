package main

import (
	"fmt"
	"os"

	csvapi "github.com/jlrcuesta/sr-backend/csvapi"
)

func main() {

	// Check if the user has passed the correct parameters
	if !hasValidParams() {
		return
	}

	// Process the csv file
	csvapi.ProcessCSVFile(os.Args[1], os.Args[2])
	return
}

func hasValidParams() bool {
	if len(os.Args) <= 1 {
		printUsage()
		return false
	} else if len(os.Args) == 2 {
		fmt.Println("Missing output filename. Please provide a valid input and output filenames.")
		printUsage()
		return false
	} else if len(os.Args) == 3 {
		return true
	} else {
		fmt.Println("Too many arguments. Please provide a valid input and output filenames.")
		printUsage()
		return false
	}
}

func printUsage() {
	fmt.Println("Usage:")
	fmt.Println("sr-backend <input_filename> <output_filename>")
}
