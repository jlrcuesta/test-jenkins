package parser

import (
	"errors"
	"strconv"

	"github.com/jlrcuesta/sr-backend/ast"
	"github.com/jlrcuesta/sr-backend/lexer"
)

// ParseCSVCellAst takes a 3D array of tokens and returns a 2D array of AstCells
func ParseCSVCellAst(csvCellTokens [][][]lexer.Token) [][]ast.AstCell {
	csvCellAst := [][]ast.AstCell{}
	for i := 0; i < len(csvCellTokens); i++ {
		astRow := []ast.AstCell{}
		for j := 0; j < len(csvCellTokens[i]); j++ {
			astCell := ParseCell(csvCellTokens[i][j], j, i, astRow)
			astRow = append(astRow, astCell)
		}
		csvCellAst = append(csvCellAst, astRow)
	}
	return csvCellAst
}

// ParseCell takes a 1D array of tokens and returns an AstCell
func ParseCell(tokens []lexer.Token, col int, row int, astRow []ast.AstCell) ast.AstCell {
	index := 0
	if tokens[index].Type == lexer.COLUMN_OPERATOR {
		return ParseColumnOperator(tokens, &index, col, row)
	} else if tokens[index].Type == lexer.EQUAL_OPERATOR {
		return ParseEqualOperator(tokens, &index, col, row, astRow)
	} else if tokens[index].Type == lexer.STRING_LITERAL || tokens[index].Type == lexer.DATE {
		if tokens[index].Type == lexer.DATE {
			return ast.CellDate{Date: tokens[index].Value, Cell: ast.Cell{Type: string(ast.CellTypeDate), Column: ast.ColumnName(col), Row: row, Error: ""}}
		} else {
			return ast.CellLiteral{Literal: tokens[index].Value, Cell: ast.Cell{Type: string(ast.CellTypeLiteral), Column: ast.ColumnName(col), Row: row, Error: ""}}
		}
	} else if tokens[index].Type == lexer.NUMBER {
		return ParseNumberOrStringStartingWithNumber(tokens, &index, col, row)
	} else if tokens[index].Type == lexer.EMPTY_STRING {
		return ast.CellExpressionEmpty{Cell: ast.Cell{Type: string(ast.CellTypeExpressionEmpty), Column: ast.ColumnName(col), Row: row, Error: ""}}
	} else {
		//TODO: Throw error
		return ast.Cell{Error: "Error parsing cell"}
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseColumnOperator(tokens []lexer.Token, index *int, col int, row int) ast.AstCell {
	*index++
	return ast.CellColumnName{ColumnName: tokens[*index].Value, Cell: ast.Cell{Type: string(ast.CellTypeColumnName), Column: ast.ColumnName(col), Row: 0, Error: ""}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseEqualOperator(tokens []lexer.Token, index *int, col int, row int, astRow []ast.AstCell) ast.AstCell {
	cellEquation := ast.CellEquation{Cell: ast.Cell{Type: string(ast.CellTypeEquation), Column: ast.ColumnName(col), Row: row, Error: ""}}
	astRow = append(astRow, cellEquation)
	*index++
	for *index < len(tokens) {
		cellEquation.Expression = ParseExpression(tokens, index, cellEquation)
	}
	return cellEquation
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseNumberOrStringStartingWithNumber(tokens []lexer.Token, index *int, col, row int) ast.AstCell {
	if len(tokens) == 1 {
		return ast.CellLiteral{Literal: tokens[*index].Value, Cell: ast.Cell{Type: string(ast.CellTypeLiteral), Column: ast.ColumnName(col), Row: 0, Error: ""}}
	} else {
		literal := ""
		for *index < len(tokens) {
			literal += tokens[*index].Value
			*index++
		}
		return ast.CellLiteral{Literal: literal, Cell: ast.Cell{Type: string(ast.CellTypeLiteral), Column: ast.ColumnName(col), Row: 0, Error: ""}}
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseCellLiteral(tokens []lexer.Token, index *int, col int, row int) ast.AstCell {
	return ast.CellLiteral{Literal: tokens[*index].Value, Cell: ast.Cell{Type: string(ast.CellTypeLiteral), Column: ast.ColumnName(col), Row: 0, Error: ""}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseCellExpressionEmpty(tokens []lexer.Token, index *int, col int, row int) ast.AstCell {
	return ast.CellExpressionEmpty{Cell: ast.Cell{Type: string(ast.CellTypeExpressionEmpty), Column: ast.ColumnName(col), Row: row, Error: ""}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseExpression(tokens []lexer.Token, index *int, astRow ast.AstElementWithExpression) ast.AstExpression {
	if tokens[*index].Type == lexer.COPY_FORMULA_OPERATOR {
		return ParseCopyFormula(tokens, index)
	} else if tokens[*index].Type == lexer.REFERENCE_CELL_OPERATOR {
		return ParseReferenceCell(tokens, index)
	} else if tokens[*index].Type == lexer.COPY_EVALUATED_FROM_ABOVE_OPERATOR {
		return ParseCopyEvaluatedFromAbove(tokens, index)
	} else if tokens[*index].Type == lexer.COPY_EVALUATED_FROM_ABOVE_IN_COLUMN_OPERATOR {
		return ParseCopyEvaluatedFromAboveInColumn(tokens, index)
	} else if tokens[*index].Type == lexer.RELATIVE_ROW_TRAVERSAL_OPERATOR {
		return ParseRelativeRowTraversal(tokens, index)
	} else if tokens[*index].Type == lexer.CONCAT || tokens[*index].Type == lexer.BTE || tokens[*index].Type == lexer.SPLIT {
		return ParseFunctionOperatorWithTwoParamsExpressions(tokens[*index].Type, tokens, index, astRow)
	} else if tokens[*index].Type == lexer.INCFROM {
		return ParseIncFromExpression(tokens, index)
	} else if tokens[*index].Type == lexer.SUM || tokens[*index].Type == lexer.SPREAD || tokens[*index].Type == lexer.TEXT {
		return ParseFunctionOperator(tokens[*index].Type, tokens, index, astRow)
	} else if tokens[*index].Type == lexer.PLUS || tokens[*index].Type == lexer.MINUS || tokens[*index].Type == lexer.MULT || tokens[*index].Type == lexer.DIV {
		return ParseArithmeticOperator(tokens[*index].Type, tokens, index, astRow)
	} else if tokens[*index].Type == lexer.OPEN_PAREN {
		return ParseParenOperator(tokens, index, astRow)
	} else if tokens[*index].Type == lexer.QUOTED_STRING {
		return ParseQuotedString(tokens, index)
	} else if tokens[*index].Type == lexer.NUMBER {
		return ParseNumberExpression(tokens, index)
	} else {
		return errors.New("Error parsing expression")
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseCopyFormula(tokens []lexer.Token, index *int) ast.AstExpression {
	*index++
	return ast.OperatorExpression{Operator: ast.CopyFormula{}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseReferenceCell(tokens []lexer.Token, index *int) ast.AstExpression {
	*index++
	colName := tokens[*index].Value
	*index++
	rowNumber, err := strconv.Atoi(tokens[*index].Value)
	if err != nil {
		return errors.New("Error parsing row number")
	}
	*index++
	return ast.OperatorExpression{Operator: ast.ReferenceCell{ColumnLetter: ast.ColumnName(colName), RowNumber: rowNumber}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseCopyEvaluatedFromAbove(tokens []lexer.Token, index *int) ast.AstExpression {
	*index++
	return ast.OperatorExpression{Operator: ast.CopyEvaluatedFromAbove{}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseCopyEvaluatedFromAboveInColumn(tokens []lexer.Token, index *int) ast.AstExpression {
	*index++
	colName := tokens[*index].Value
	*index++
	return ast.OperatorExpression{Operator: ast.CopyEvaluatedFromAboveInColumn{Column: ast.ColumnName(colName)}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseRelativeRowTraversal(tokens []lexer.Token, index *int) ast.AstExpression {
	*index++
	label := tokens[*index].Value
	*index++
	rowNumber, err := strconv.Atoi(tokens[*index].Value)
	if err != nil {
		return errors.New("Error parsing row number")
	}
	*index++
	return ast.OperatorExpression{Operator: ast.RelativeRowTraversal{Label: label, Row: rowNumber}}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseIncFromExpression(tokens []lexer.Token, index *int) ast.AstExpression {
	*index++
	if tokens[*index].Type == lexer.OPEN_PAREN {
		*index++
		inc, err := strconv.Atoi(tokens[*index].Value)
		if err != nil {
			return errors.New("Error parsing row number")
		}
		*index++
		if tokens[*index].Type == lexer.CLOSE_PAREN {
			*index++
			incFromOperator := ast.IncFromOperator{Param: inc}
			return ast.OperatorExpression{Operator: incFromOperator}
		} else {
			return errors.New("Error parsing expression")
		}
	} else {
		return errors.New("Error parsing expression")
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
// TODO: Refactor this function and ParseFunctionOperator in a new fucntion that has an int param with the nunmber of param expressions
func ParseFunctionOperatorWithTwoParamsExpressions(tokenType lexer.TokenType, tokens []lexer.Token, index *int, astRow ast.AstElementWithExpression) ast.AstExpression {
	*index++
	if tokens[*index].Type == lexer.OPEN_PAREN {
		*index++
		expression := ParseExpression(tokens, index, astRow)
		if tokens[*index].Type == lexer.COMA {
			*index++
			expression2 := ParseExpression(tokens, index, astRow)
			if tokens[*index].Type == lexer.CLOSE_PAREN {
				*index++
				concatOperator := GetOperatorWithTwoParamsFromToken(tokenType, expression, expression2)
				return ast.OperatorExpression{Operator: concatOperator}
			} else {
				return errors.New("Error parsing expression")
			}
		} else {
			return errors.New("Error parsing expression")
		}
	} else {
		return errors.New("Error parsing expression")
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseFunctionOperator(tokenType lexer.TokenType, tokens []lexer.Token, index *int, astRow ast.AstElementWithExpression) ast.AstExpression {
	*index++
	if tokens[*index].Type == lexer.OPEN_PAREN {
		*index++
		expression := ParseExpression(tokens, index, astRow)
		if tokens[*index].Type == lexer.CLOSE_PAREN {
			*index++
			sumOperator := ast.SumOperator{Param: expression}
			return ast.OperatorExpression{Operator: sumOperator}
		} else {
			return errors.New("Error parsing expression")
		}
	} else {
		return errors.New("Error parsing expression")
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseArithmeticOperator(tokenType lexer.TokenType, tokens []lexer.Token, index *int, astRow ast.AstElementWithExpression) ast.AstExpression {
	*index++
	expression1 := GetExpressionFromAstElementWithExpression(astRow)
	expression2 := ParseExpression(tokens, index, astRow)
	return ast.ArithmeticExpression{Operator: GetArithmeticOperatorFromToken(tokenType, expression1, expression2)}
}

func GetExpressionFromAstElementWithExpression(astRow ast.AstElementWithExpression) ast.AstExpression {
	switch astRow.(type) {
	case ast.CellEquation:
		return astRow.(ast.CellEquation).Expression
	case ast.ParenExpression:
		return astRow.(ast.ParenExpression).Expression
	default:
		return errors.New("Error parsing expression")
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseParenOperator(tokens []lexer.Token, index *int, astRow ast.AstElementWithExpression) ast.AstExpression {
	*index++
	parenExpression := ast.ParenExpression{Expression: ParseExpression(tokens, index, astRow)}
	for tokens[*index].Type != lexer.CLOSE_PAREN {
		parenExpression.Expression = ParseExpression(tokens, index, parenExpression)
	}
	if tokens[*index].Type == lexer.CLOSE_PAREN {
		*index++
		return parenExpression
	} else {
		return errors.New("Error parsing expression")
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseQuotedString(tokens []lexer.Token, index *int) ast.AstExpression {
	*index++
	if tokens[*index].Type == lexer.STRING_LITERAL {
		quotedString := tokens[*index].Value
		*index++
		return ast.QuotedStringExpression{QuotedString: quotedString}
	} else {
		return errors.New("Error parsing expression")
	}
}

// ParseExpression takes a 1D array of tokens and returns an AstExpression
func ParseNumberExpression(tokens []lexer.Token, index *int) ast.AstExpression {
	value, err := strconv.ParseFloat(tokens[*index].Value, 64)
	*index++
	if err != nil {
		return errors.New("Error parsing row number")
	}
	return ast.NumberExpression{Value: value}
}

// Returns an AstOperator from a token
func GetOperatorWithTwoParamsFromToken(tokenType lexer.TokenType, expression1 ast.AstExpression, expression2 ast.AstExpression) ast.AstOperator {
	switch tokenType {
	case lexer.CONCAT:
		return ast.ConcatOperator{Param1: expression1, Param2: expression2}
	case lexer.BTE:
		return ast.BteOperator{Param1: expression1, Param2: expression2}
	case lexer.SPLIT:
		return ast.SplitOperator{Param1: expression1, Param2: expression2}
	default:
		return errors.New("Error parsing expression")
	}
}

// Returns an AstOperator from a token
func GetOperatorFromToken(tokenType lexer.TokenType, expression ast.AstExpression) ast.AstOperator {
	switch tokenType {
	case lexer.SUM:
		return ast.SumOperator{Param: expression}
	case lexer.SPREAD:
		return ast.SpreadOperator{Param: expression}
	case lexer.TEXT:
		return ast.TextOperator{Param: expression}
	default:
		return errors.New("Error parsing expression")
	}
}

// Returns an AstOperator from a token
func GetArithmeticOperatorFromToken(tokenType lexer.TokenType, expression1, expression2 ast.AstExpression) ast.AstOperator {
	switch tokenType {
	case lexer.PLUS:
		return ast.PlusOperator{Param1: expression1, Param2: expression2}
	case lexer.MINUS:
		return ast.MinusOperator{Param1: expression1, Param2: expression2}
	case lexer.MULT:
		return ast.MultOperator{Param1: expression1, Param2: expression2}
	case lexer.DIV:
		return ast.DivOperator{Param1: expression1, Param2: expression2}
	default:
		return errors.New("Error parsing expression")
	}
}
