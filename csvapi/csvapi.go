package csvapi

import (
	"os"

	"github.com/jlrcuesta/sr-backend/csvutils"
	"github.com/jlrcuesta/sr-backend/interpreter"
	"github.com/jlrcuesta/sr-backend/lexer"
	"github.com/jlrcuesta/sr-backend/parser"
)

func ProcessCSVFile(inputFileName string, outputFileNam string) {

	// Read csv file and slice it into a 2D array
	csvArray := csvutils.ReadCsvFile(os.Args[1])

	// Scan each cell of the array and tokenize it into a 3D array of tokens
	csvCellTokens := lexer.GetCSVCellTokens(csvArray)

	// Read each cell of the array and parse it into a Abstract Syntax Tree
	astCell := parser.ParseCSVCellAst(csvCellTokens)

	// Interpret each cell Abstract Syntax Tree and generate an array with all the values generated
	csvResult := interpreter.Interpret(astCell)

	// Write the interpreted array into a csv file
	csvutils.WriteCsvFile(os.Args[2], csvResult)
}
