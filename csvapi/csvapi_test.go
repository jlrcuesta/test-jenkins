package csvapi

import (
	"reflect"
	"testing"

	"github.com/jlrcuesta/sr-backend/csvutils"
)

func TestProcessCSVFile(t *testing.T) {
	ProcessCSVFile("test_resources/test.csv", "test_resources/test_result.csv")
	got := csvutils.ReadCsvFile("resources/test_result.csv")
	want := [][]string{{"date", "transaction_id", "tokens", "token_prices", "total_cost"},
		{"2022-02-20", "t_2022-02-20", "btc,eth,dai", "38341.88,2643.77,1.0003", "40986.6503"},
		{"2022-02-21", "t_2022-02-20", "bch,eth,dai", "304.38,2621.15,1.0001", "43913.1804"},
		{"2022-02-22", "t_2022-02-20", "sol,eth,dai", "85,2604.17,0.9997", "43913.1804"},
		{"fee", "cost_threshold", "", "", ""},
		{"0.09", "10000", "", "", ""},
		{"adjusted_cost", "", "", "", ""},
		{"47865.366636", "", "", "", ""},
		{"cost_too_high", "", "", "4", ""},
		{"37865.366636", "", "", "", ""},
	}

	if reflect.DeepEqual(got, want) {
		t.Errorf("got %q, wanted %q", got, want)
	}
}
