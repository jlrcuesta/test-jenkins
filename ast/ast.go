package ast

type ColumnName string

const (
	A ColumnName = "A"
	B ColumnName = "B"
	C ColumnName = "C"
	D ColumnName = "D"
	E ColumnName = "E"
	F ColumnName = "F"
	G ColumnName = "G"
	H ColumnName = "H"
	I ColumnName = "I"
	J ColumnName = "J"
	K ColumnName = "K"
	L ColumnName = "L"
	M ColumnName = "M"
	N ColumnName = "N"
	O ColumnName = "O"
	P ColumnName = "P"
	Q ColumnName = "Q"
	R ColumnName = "R"
	S ColumnName = "S"
	T ColumnName = "T"
	U ColumnName = "U"
	V ColumnName = "V"
	W ColumnName = "W"
	X ColumnName = "X"
	Y ColumnName = "Y"
	Z ColumnName = "Z"
)

type CellType string

const (
	CellTypeEquation        CellType = "Equation"
	CellTypeColumnName      CellType = "ColumnName"
	CellTypeLiteral         CellType = "Literal"
	CellTypeDate            CellType = "Date"
	CellTypeExpressionEmpty CellType = "ExpressionEmpty"
)

type AstCell interface {
}

type AstElementWithExpression interface {
}

type Cell struct {
	Type   string
	Column ColumnName
	Row    int
	Error  string
}

type CellEquation struct {
	Expression AstExpression
	Cell
}

type CellColumnName struct {
	ColumnName string
	Cell
}

type CellLiteral struct {
	Literal string
	Cell
}

type CellDate struct {
	Date string
	Cell
}

type CellExpressionEmpty struct {
	Cell
}

type AstExpression interface {
}

type OperatorExpression struct {
	Operator AstOperator
}

type ArithmeticExpression struct {
	Operator AstOperator
}

type ParenExpression struct {
	Expression AstExpression
}

type QuotedStringExpression struct {
	QuotedString string
}

type NumberExpression struct {
	Value float64
}

type AstOperator interface {
}

type CopyFormula struct {
}

type ReferenceCell struct {
	ColumnLetter ColumnName
	RowNumber    int
}

type CopyEvaluatedFromAbove struct {
}

type CopyEvaluatedFromAboveInColumn struct {
	Column ColumnName
}

type RelativeRowTraversal struct {
	Label string
	Row   int
}

type ConcatOperator struct {
	Param1 AstExpression
	Param2 AstExpression
}

type IncFromOperator struct {
	Param int
}

type SumOperator struct {
	Param AstExpression
}

type SpreadOperator struct {
	Param AstExpression
}

type SplitOperator struct {
	Param1 AstExpression
	Param2 AstExpression
}

type TextOperator struct {
	Param AstExpression
}

type BteOperator struct {
	Param1 AstExpression
	Param2 AstExpression
}

type PlusOperator struct {
	Param1 AstExpression
	Param2 AstExpression
}

type MultOperator struct {
	Param1 AstExpression
	Param2 AstExpression
}

type DivOperator struct {
	Param1 AstExpression
	Param2 AstExpression
}

type MinusOperator struct {
	Param1 AstExpression
	Param2 AstExpression
}

type ParenOperator struct {
	Param AstExpression
}
