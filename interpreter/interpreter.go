package interpreter

import (
	"fmt"
	"math"
	"strconv"
	"strings"

	"github.com/jlrcuesta/sr-backend/ast"
)

type ColumnLabelElement struct {
	row int
	col int
}

var columnNameMap = map[ast.ColumnName]int{
	ast.A: 0,
	ast.B: 1,
	ast.C: 2,
	ast.D: 3,
	ast.E: 4,
	ast.F: 5,
	ast.G: 6,
	ast.H: 7,
	ast.I: 8,
	ast.J: 9,
	ast.K: 10,
	ast.L: 11,
	ast.M: 12,
	ast.N: 13,
	ast.O: 14,
	ast.P: 15,
	ast.Q: 16,
	ast.R: 17,
	ast.S: 18,
	ast.T: 19,
	ast.U: 20,
	ast.V: 21,
	ast.W: 22,
	ast.X: 23,
	ast.Y: 24,
	ast.Z: 25,
}

var columnLabelMap = map[string]ColumnLabelElement{}

func Interpret(csvAst [][]ast.AstCell) [][]string {

	// Initialize columnLabelMap
	for i := 0; i < len(csvAst); i++ {
		for j := 0; j < len(csvAst[i]); j++ {
			if _, ok := csvAst[i][j].(ast.CellColumnName); ok {
				columnLabelMap[csvAst[i][j].(ast.CellColumnName).ColumnName] = ColumnLabelElement{i, j}
			}
		}
	}

	// Interpret each cell of the csvAst
	fmt.Println("Interpreting...")
	csvResult := make([][]string, 0)
	for i := 0; i < len(csvAst); i++ {
		csvResultRow := make([]string, 0)
		for j := 0; j < len(csvAst[i]); j++ {
			result := InterpretAstCell(csvAst[i][j], i, j, csvResult, csvAst, csvResultRow)
			csvResultRow = append(csvResultRow, result)
		}
		csvResult = append(csvResult, csvResultRow)
	}
	return csvResult
}

func InterpretAstCell(cell ast.AstCell, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	switch cell.(type) {
	case ast.CellEquation:
		return InterpretCellEquation(cell.(ast.CellEquation), row, col, csvResult, csvAst, csvResultRow)
	case ast.CellColumnName:
		return InterpretCellColumnName(cell.(ast.CellColumnName))
	case ast.CellLiteral:
		return InterpretCellLiteral(cell.(ast.CellLiteral))
	case ast.CellDate:
		return InterpretCellDate(cell.(ast.CellDate))
	case ast.CellExpressionEmpty:
		return InterpretCellExpressionEmpty(cell.(ast.CellExpressionEmpty))
	default:
		return ""
	}
}

func InterpretCellEquation(cellEquation ast.CellEquation, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	return InterpretAstExpression(cellEquation.Expression, row, col, csvResult, csvAst, csvResultRow)
}

func InterpretAstExpression(astExpression ast.AstExpression, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	switch astExpression.(type) {
	case ast.OperatorExpression:
		return InterpretOperatorExpression(astExpression.(ast.OperatorExpression), row, col, csvResult, csvAst, csvResultRow)
	case ast.ArithmeticExpression:
		return InterpretArithmeticExpression(astExpression.(ast.ArithmeticExpression), row, col, csvResult, csvAst, csvResultRow)
	case ast.ParenExpression:
		return InterpretParenExpression(astExpression.(ast.ParenExpression), row, col, csvResult, csvAst, csvResultRow)
	case ast.QuotedStringExpression:
		return InterpretQuotedStringExpression(astExpression.(ast.QuotedStringExpression))
	case ast.NumberExpression:
		return InterpretNumberExpression(astExpression.(ast.NumberExpression))
	default:
		return ""
	}
}

func InterpretOperatorExpression(operatorExpression ast.OperatorExpression, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	switch operatorExpression.Operator.(type) {
	case ast.CopyFormula:
		return InterpretCopyFormula(operatorExpression.Operator.(ast.CopyFormula), row, col, csvResult)
	case ast.ReferenceCell:
		return InterpretReferenceCell(operatorExpression.Operator.(ast.ReferenceCell), csvResult, csvResultRow)
	case ast.CopyEvaluatedFromAbove:
		return InterpretCopyEvaluatedFromAbove(operatorExpression.Operator.(ast.CopyEvaluatedFromAbove), row, col, csvResult, csvAst, csvResultRow)
	case ast.CopyEvaluatedFromAboveInColumn:
		return InterpretCopyEvaluatedFromAboveInColumn(operatorExpression.Operator.(ast.CopyEvaluatedFromAboveInColumn), row, col, csvResult, csvAst, csvResultRow)
	case ast.RelativeRowTraversal:
		return InterpretRelativeRowTraversal(operatorExpression.Operator.(ast.RelativeRowTraversal), csvResult, csvResultRow)
	case ast.ConcatOperator:
		return InterpretConcatOperator(operatorExpression.Operator.(ast.ConcatOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.IncFromOperator:
		return InterpretIncFromOperator(operatorExpression.Operator.(ast.IncFromOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.SumOperator:
		return InterpretSumOperator(operatorExpression.Operator.(ast.SumOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.SpreadOperator:
		return InterpretSpreadOperator(operatorExpression.Operator.(ast.SpreadOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.SplitOperator:
		return InterpretSplitOperator(operatorExpression.Operator.(ast.SplitOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.TextOperator:
		return InterpretTextOperator(operatorExpression.Operator.(ast.TextOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.BteOperator:
		return InterpretBteOperator(operatorExpression.Operator.(ast.BteOperator), row, col, csvResult, csvAst, csvResultRow)
	default:
		return ""
	}
}

func InterpretArithmeticExpression(arithmeticExpression ast.ArithmeticExpression, row, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	switch arithmeticExpression.Operator.(type) {
	case ast.PlusOperator:
		return InterpretPlusOperator(arithmeticExpression.Operator.(ast.PlusOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.MinusOperator:
		return InterpretMinusOperator(arithmeticExpression.Operator.(ast.MinusOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.MultOperator:
		return InterpretMultOperator(arithmeticExpression.Operator.(ast.MultOperator), row, col, csvResult, csvAst, csvResultRow)
	case ast.DivOperator:
		return InterpretDivOperator(arithmeticExpression.Operator.(ast.DivOperator), row, col, csvResult, csvAst, csvResultRow)
	default:
		return ""
	}
}

func InterpretCopyFormula(copyFormula ast.CopyFormula, row int, col int, csvResult [][]string) string {
	return csvResult[row-1][col]
}

func InterpretReferenceCell(referenceCell ast.ReferenceCell, csvResult [][]string, csvResultRow []string) string {
	refRow := referenceCell.RowNumber - 1
	if refRow >= len(csvResult) {
		return csvResultRow[columnNameMap[referenceCell.ColumnLetter]]
	} else {
		return csvResult[referenceCell.RowNumber-1][columnNameMap[referenceCell.ColumnLetter]]
	}
}

func InterpretCopyEvaluatedFromAbove(copyEvaluatedFromAbove ast.CopyEvaluatedFromAbove, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	index := row - 1
	for index >= 0 {
		if _, ok := csvAst[index][col].(ast.CellEquation); ok {
			return csvResult[index][col]
		}
		index--
	}
	return "0"
}

func InterpretCopyEvaluatedFromAboveInColumn(copyEvaluatedFromAboveInColumn ast.CopyEvaluatedFromAboveInColumn, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	index := row - 1
	columnFrom := columnNameMap[copyEvaluatedFromAboveInColumn.Column]
	for index >= 0 {
		if _, ok := csvAst[index][columnFrom].(ast.CellEquation); ok {
			return csvResult[index][columnFrom]
		}
		index--
	}
	return "0"
}

func InterpretRelativeRowTraversal(relativeRowTraversal ast.RelativeRowTraversal, csvResult [][]string, csvResultRow []string) string {
	labelRow := columnLabelMap[relativeRowTraversal.Label].row
	if labelRow >= len(csvResult) {
		return csvResultRow[columnLabelMap[relativeRowTraversal.Label].col+relativeRowTraversal.Row]
	} else {
		return csvResult[labelRow+relativeRowTraversal.Row][columnLabelMap[relativeRowTraversal.Label].col]
	}
}

func InterpretConcatOperator(concatOperator ast.ConcatOperator, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	return InterpretAstExpression(concatOperator.Param1, row, col, csvResult, csvAst, csvResultRow) + InterpretAstExpression(concatOperator.Param2, row, col, csvResult, csvAst, csvResultRow)
}

func InterpretIncFromOperator(incFromOperator ast.IncFromOperator, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	return csvResultRow[incFromOperator.Param-1]
}

// TODO: The sume operator will rely on the inner split operator to perform the sum
func InterpretSumOperator(sumOperator ast.SumOperator, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	return InterpretAstExpression(sumOperator.Param, row, col, csvResult, csvAst, csvResultRow)
}

// TODO: The spread operator will rely on the inner split operator to perform the sum
func InterpretSpreadOperator(spreadOperator ast.SpreadOperator, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	return InterpretAstExpression(spreadOperator.Param, row, col, csvResult, csvAst, csvResultRow)
}

// TODO: For the time being, the split operator will perform a sum of the numbers in the string as sum(spread(split(Param1, Param2))) are usually together
// If the split operator is used in more sophisiacted ways, this will need to be changed
func InterpretSplitOperator(splitOperator ast.SplitOperator, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	ss := strings.Split(InterpretAstExpression(splitOperator.Param1, row, col, csvResult, csvAst, csvResultRow), InterpretAstExpression(splitOperator.Param2, row, col, csvResult, csvAst, csvResultRow))
	sum := 0.0
	for i := 0; i < len(ss); i++ {
		val, _ := strconv.ParseFloat(ss[i], 64)
		sum = sum + val
	}
	sum = math.Round(sum*10000) / 10000
	return strconv.FormatFloat(sum, 'f', -1, 64)
}

func InterpretTextOperator(textOperator ast.TextOperator, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	return InterpretAstExpression(textOperator.Param, row, col, csvResult, csvAst, csvResultRow)
}

// TODO: Figure out what is the bte function. For now, I will implement a simple substaction of the params
func InterpretBteOperator(bteOperator ast.BteOperator, row int, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	param1, _ := strconv.ParseFloat(InterpretAstExpression(bteOperator.Param1, row, col, csvResult, csvAst, csvResultRow), 64)
	param2, _ := strconv.ParseFloat(InterpretAstExpression(bteOperator.Param2, row, col, csvResult, csvAst, csvResultRow), 64)
	return strconv.FormatFloat(param1-param2, 'f', -1, 64)
}

func InterpretPlusOperator(plusOperator ast.PlusOperator, row, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	param1, _ := strconv.ParseFloat(InterpretAstExpression(plusOperator.Param1, row, col, csvResult, csvAst, csvResultRow), 64)
	param2, _ := strconv.ParseFloat(InterpretAstExpression(plusOperator.Param2, row, col, csvResult, csvAst, csvResultRow), 64)
	return strconv.FormatFloat(param1+param2, 'f', -1, 64)
}

func InterpretMinusOperator(minusOperator ast.MinusOperator, row, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	param1, _ := strconv.ParseFloat(InterpretAstExpression(minusOperator.Param1, row, col, csvResult, csvAst, csvResultRow), 64)
	param2, _ := strconv.ParseFloat(InterpretAstExpression(minusOperator.Param2, row, col, csvResult, csvAst, csvResultRow), 64)
	return strconv.FormatFloat(param1-param2, 'f', -1, 64)
}

func InterpretMultOperator(multOperator ast.MultOperator, row, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	param1, _ := strconv.ParseFloat(InterpretAstExpression(multOperator.Param1, row, col, csvResult, csvAst, csvResultRow), 64)
	param2, _ := strconv.ParseFloat(InterpretAstExpression(multOperator.Param2, row, col, csvResult, csvAst, csvResultRow), 64)
	return strconv.FormatFloat(param1*param2, 'f', -1, 64)
}

func InterpretDivOperator(divOperator ast.DivOperator, row, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	param1, _ := strconv.ParseFloat(InterpretAstExpression(divOperator.Param1, row, col, csvResult, csvAst, csvResultRow), 64)
	param2, _ := strconv.ParseFloat(InterpretAstExpression(divOperator.Param2, row, col, csvResult, csvAst, csvResultRow), 64)
	return strconv.FormatFloat(param1/param2, 'f', -1, 64)
}

func InterpretParenExpression(parenExpression ast.ParenExpression, row, col int, csvResult [][]string, csvAst [][]ast.AstCell, csvResultRow []string) string {
	return InterpretAstExpression(parenExpression.Expression, row, col, csvResult, csvAst, csvResultRow)
}

func InterpretQuotedStringExpression(quotedStringExpression ast.QuotedStringExpression) string {
	return quotedStringExpression.QuotedString
}

func InterpretNumberExpression(numberExpression ast.NumberExpression) string {
	return strconv.Itoa(int(numberExpression.Value))
}

func InterpretCellColumnName(cellColumnName ast.CellColumnName) string {
	return cellColumnName.ColumnName
}

func InterpretCellLiteral(cellLiteral ast.CellLiteral) string {
	return cellLiteral.Literal
}

func InterpretCellDate(cellDate ast.CellDate) string {
	return cellDate.Date
}

func InterpretCellExpressionEmpty(cellExpressionEmpty ast.CellExpressionEmpty) string {
	return ""
}
