package lexer

import (
	"regexp"
)

// TokenType is an enum for the type of token
type TokenType int

const (
	COLUMN_OPERATOR TokenType = iota
	EQUAL_OPERATOR
	COPY_FORMULA_OPERATOR
	REFERENCE_CELL_OPERATOR
	COPY_EVALUATED_FROM_ABOVE_IN_COLUMN_OPERATOR
	COPY_EVALUATED_FROM_ABOVE_OPERATOR
	RELATIVE_ROW_TRAVERSAL_OPERATOR
	CONCAT
	INCFROM
	SUM
	SPREAD
	SPLIT
	TEXT
	BTE
	PLUS
	MULT
	DIV
	MINUS
	OPEN_PAREN
	CLOSE_PAREN
	COMA
	QUOTED_STRING
	WHITESPACE
	DATE
	NUMBER
	COLUMN_NAME
	STRING_LITERAL
	EMPTY_STRING
)

// tokenTypeRegex is an array of regex for each token type
var tokenTypeRegex = []string{
	"\\!(.+)",                          // COLUMN_OPERATOR
	"\\=",                              // EQUAL_OPERATOR
	"\\^\\^",                           // COPY_FORMULA_OPERATOR
	"([A-Z])([1-9]|[1-9][0-9]|100)",    // REFERENCE_CELL_OPERATOR
	"([A-Z])\\^v",                      // COPY_EVALUATED_FROM_ABOVE_IN_COLUMN_OPERATOR
	"[A-Z]\\^",                         // COPY_EVALUATED_FROM_ABOVE_OPERATOR
	"\\@([A-Za-z0-9_]*)\\<([0-9]*)\\>", // RELATIVE_ROW_TRAVERSAL_OPERATOR
	"concat\\(",                        // CONCAT
	"incFrom\\(",                       // INCFROM
	"sum\\(",                           // SUM
	"spread\\(",                        // SPREAD
	"split\\(",                         // SPLIT
	"text\\(",                          // TEXT
	"bte\\(",                           // BTE
	"\\+",                              // PLUS
	"\\*",                              // MULT
	"\\/",                              // DIV
	"\\-",                              // MINUS
	"\\(",                              // OPEN_PAREN
	"\\)",                              // CLOSE_PAREN
	"\\,",                              // COMA
	"\"(.+)\"",                         // QUOTED_STRING
	"\\s+",                             // WHITESPACE
	"\\d{4}-\\d{2}-\\d{2}",             // DATE
	"^[-]?([1-9]{1}[0-9]{0,}(\\.[0-9]{0,2})?|0(\\.[0-9]{0,2})?|\\.[0-9]{1,2})", // NUMBER
	"[A-Z]",                          // COLUMN_NAME
	"[^\\(\\)\\+\\-\\*\\/\"\\!\\=]+", // STRING_LITERAL
}

// Token is a struct that represents a token
type Token struct {
	Type  TokenType
	Value string
}

// Method that returns an array of compiled regexes for each token type
func GetTokensRegex() []*regexp.Regexp {
	tokensRegexes := []*regexp.Regexp{}
	for i := 0; i < len(tokenTypeRegex); i++ {
		tokensRegexes = append(tokensRegexes, regexp.MustCompile(tokenTypeRegex[i]))
	}
	return tokensRegexes
}

// Method that returns the next token in the cell value
func GetNextToken(cellValue string, index *int, regexes []*regexp.Regexp) []Token {
	substr := cellValue[*index:]
	for i := COLUMN_OPERATOR; i <= STRING_LITERAL; i++ {
		result := regexes[i].FindStringSubmatch(substr)
		// We check if there is match and if the match starts at the beginning of the cell value
		if len(result) > 0 && regexes[i].FindStringIndex(substr)[0] == 0 {
			if i == COLUMN_OPERATOR {
				*index += len(result[0])
				return []Token{{COLUMN_OPERATOR, result[0]}, {STRING_LITERAL, result[1]}}
			} else if i == REFERENCE_CELL_OPERATOR {
				*index += len(result[0])
				return []Token{{REFERENCE_CELL_OPERATOR, result[0]}, {COLUMN_NAME, result[1]}, {NUMBER, result[2]}}
			} else if i == COPY_EVALUATED_FROM_ABOVE_IN_COLUMN_OPERATOR {
				*index += len(result[0])
				return []Token{{COPY_EVALUATED_FROM_ABOVE_IN_COLUMN_OPERATOR, result[0]}, {COLUMN_NAME, result[1]}}
			} else if i == RELATIVE_ROW_TRAVERSAL_OPERATOR {
				*index += len(result[0])
				return []Token{{RELATIVE_ROW_TRAVERSAL_OPERATOR, result[0]}, {STRING_LITERAL, result[1]}, {NUMBER, result[2]}}
			} else if i == CONCAT || i == INCFROM || i == SUM || i == SPREAD || i == SPLIT || i == TEXT || i == BTE {
				*index += len(result[0]) - 1
				return []Token{{i, result[0]}}
			} else if i == QUOTED_STRING {
				*index += len(result[0])
				return []Token{{QUOTED_STRING, result[0]}, {STRING_LITERAL, result[1]}}
			} else if i == WHITESPACE {
				*index += len(result[0])
				return []Token{{WHITESPACE, result[0]}}
			} else {
				*index += len(result[0])
				return []Token{{i, result[0]}}
			}
		}
	}

	// In this case we have a string literal
	//*index += len(substr)
	//return []Token{{STRING_LITERAL, substr}}
	return nil
}

// Method that returns an array of tokens for a given cell value
func GetTokens(cellValue string, regexes []*regexp.Regexp) []Token {
	if cellValue == "" {
		return []Token{{EMPTY_STRING, ""}}
	}
	tokens := []Token{}
	index := 0
	for index < len(cellValue) {
		token := GetNextToken(cellValue, &index, regexes)
		if token[0].Type != WHITESPACE {
			tokens = append(tokens, token...)
		}
	}
	return tokens
}

// Method that returns an array of tokens for each cell in the csv
func GetCSVCellTokens(csvArray [][]string) [][][]Token {
	regexes := GetTokensRegex()
	csvCellTokens := [][][]Token{}
	for i := 0; i < len(csvArray); i++ {
		row := [][]Token{}
		for j := 0; j < len(csvArray[i]); j++ {
			tokens := GetTokens(csvArray[i][j], regexes)
			row = append(row, tokens)
		}
		csvCellTokens = append(csvCellTokens, row)
	}
	return csvCellTokens
}
